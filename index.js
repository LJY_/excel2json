// 'use strict';
const excelToJson = require('convert-excel-to-json');
var jsonfile = require('jsonfile');

//엑셀파일
var fs = require('fs');

function getDataSheet(excelFile, filePath){
    console.log(excelFile, filePath)
    const excel_sheets = excelToJson({
        sourceFile: excelFile,
        // header: {
        //     rows: 2 //몇번째 줄부터 시작인지?
        // },
        // //원하는 시트만 골라서 뽑아내기
        sheets: [
            {
                name: 'book',
                header: {
                    rows: 1
                }
            },
            {
                name: 'curriculum',
                header: {
                    rows: 1
                }
            },
            {
                name: 'details',
                header: {
                    rows: 2
                }
            },
            {
                name: 'closing',
                header: {
                    rows: 1
                }
            }
            // 'book', 'curriculum', 'details', 'closing'
        ]
    });
    // console.log('엑셀시트', excel_sheets);
 

    //input_data에 모든 시트의 데이터를 삽입
    let input_data = [];
    input_data = [Object.assign(
        getDataListFromSheet("book", excel_sheets.book),
        getDataListFromSheet("curriculum", excel_sheets.curriculum),
        getDataListFromSheet("details", excel_sheets.details),
        getDataListFromSheet("closing", excel_sheets.closing)
    )];
      
    console.log('과연',input_data);
    
    if (!fs.existsSync(filePath)){
        fs.mkdirSync(filePath);
    }

    jsonfile.writeFile(filePath+'/epub_library.json', input_data);

    return input_data;
}

//시트별 JSON 재구성
function getDataListFromSheet(sheet_name, input_sheet1){
    const regex = /page/gi;
    
    /*
    book sheet
    학년군/학년 - grade(string): "junior" | "middle" | "senior"
    열기버전 - opversion(string): "op1" | "op2" ...
    도움말 - helptext(string): "h1" | "h2" | "h3" | "h4" - 뷰어 도움말 버전
    책제목 - title(string)
    성우음성 - readingaudio(string) - 뷰어 음성있는지 여부
    파일명 - key(string) - 차시코드
    글 - author(string)
    그림 - artist(string)
    출판사 - publisher(string)
    책 분류 - classification(string)
    책 소개글 - introduction(string) - 책열기에서 사용
    책 소개 음성 파일명 - bookintrovoice(string) - 책열기에서 사용

    curriculum sheet
    교과연계 책 - textbooklink.link(string) - 책열기에서 사용
    교과연계 챕터 제목 - textbooklink.chapter(string) - 책열기에서 사용

    details sheet
    수상 - details.include(string) - 뷰어에서 사용
    수상 내용 - details.contents(string) - 뷰어에서 사용

    closing sheet
    책 읽기 책 제목 음성 파일명 - narr - 뷰어 첫 표지에서 재생되는 음성
    총 분권 개수 - dividedbooknumber(number)
    현재 분권 번호 - currentdivided(number)
    클로징 멘트 - closingment(string) - 뷰어 맨마지막에서 재생되는 음성 멘트
    클로징 음성 파일명 - closingaudio(string) - 뷰어 맨마지막에서 재생되는 음성

    미사용
    idx ??
    ib - 교과서 수록 학기정보
    ibc - 교과서 수록 챕터정보
    */
    //한 줄 분의 데이터 작성

    if(sheet_name === 'book'){
        let target_data = {};
        input_sheet1.forEach(input_line => {
            const line_data = {};

            if(input_line.A){
                if(input_line.A === '저학년'){
                    line_data.grade = 'junior';
                }else if(input_line.A === '중학년'){
                    line_data.grade = 'middle';
                }else if(input_line.A === '고학년'){
                    line_data.grade = 'senior';
                }
            }else{
                console.error('ERROR:: book sheet A열 누락');
                line_data.grade = '';
            }

            if(input_line.B){
                line_data.opversion = input_line.B;
            }else{
                console.error('ERROR:: book sheet B열 누락');
                line_data.opversion = '';
            }
            
            if(input_line.C){
                line_data.helptext = input_line.C;
            }else{
                console.error('ERROR:: book sheet C열 누락');
                line_data.helptext = 'op1';
            }
            
            if(input_line.D){
                line_data.title = input_line.D;
            }else{
                console.error('ERROR:: book sheet D열 누락');
                line_data.title = '';
            }
            
            if(input_line.E){
                if(input_line.E.trim() == '있음'){
                    line_data.readingaudio = "true";
                }else if(input_line.E.trim() == '없음'){
                    line_data.readingaudio = "false";
                }else{
                    console.error('ERROR:: book sheet E열 누락');
                    line_data.readingaudio = 'false';
                }
            }else{
                console.error('ERROR:: book sheet E열 누락');
                line_data.readingaudio = 'false';
            }

            if(input_line.F){
                line_data.key = input_line.F;
            }else{
                console.error('ERROR:: book sheet F열 누락');
                line_data.key = '';
            }
            
            if(input_line.G){
                line_data.author = input_line.G;
            }else{
                console.error('ERROR:: book sheet G열 누락');
                line_data.author = '';
            }
            
            if(input_line.H){
                line_data.artist = input_line.H;
            }else{
                console.error('ERROR:: book sheet H열 누락');
                line_data.artist = '';
            }
            
            if(input_line.I){
                line_data.publisher = input_line.I;
            }else{
                console.error('ERROR:: book sheet I열 누락');
                line_data.publisher = '';
            }
            
            if(input_line.J){
                line_data.classification = input_line.J;
            }else{
                console.error('ERROR:: book sheet J열 누락');
                line_data.classification = '';
            }
            
            if(input_line.K){
                line_data.introduction = input_line.K;
            }else{
                console.error('ERROR:: book sheet K열 누락');
                line_data.introduction = '';
            }
            
            if(input_line.L){
                line_data.bookintrovoice = input_line.L;
            }else{
                console.error('ERROR:: book sheet L열 누락');
                line_data.bookintrovoice = '';
            }
      
            //한 줄 분의 데이터를 시트 전체의 데이터에 삽입
            // target_data.push(line_data);
            target_data = line_data;
        });
            
        return target_data;
    }else if(sheet_name === 'curriculum'){
        let target_data = [];
        input_sheet1.forEach(input_line => {
            const line_data = {};

            if(input_line.A){
                line_data.link = input_line.A;
            }else{
                console.error('ERROR:: curriculum sheet '+index+'행 A열 누락');
                line_data.link = '';
            }
            
            if(input_line.B){
                line_data.chapter = input_line.B;
            }else{
                console.error('ERROR:: curriculum sheet '+index+'행 B열 누락');
                line_data.chapter = '';
            }
            
            target_data.push(line_data)
        });
        
        return {textbooklink: target_data};
    }else if(sheet_name === 'details'){
        let target_data = [];
        input_sheet1.forEach((input_line, index) => {
            const line_data = {};

            if(input_line.A){
                line_data.include = input_line.A;
            }else{
                console.error('ERROR:: details sheet '+index+'행 A열 누락');
                line_data.include = '';
            }
            
            if(input_line.B){
                line_data.contents = input_line.B;
            }else{
                console.error('ERROR:: details sheet '+index+'행 B열 누락');
                line_data.contents = '';
            }
            
            target_data.push(line_data)
        });
        
        return {details: target_data};
    }else if(sheet_name === 'closing'){
        let target_data = {};

        input_sheet1.forEach((input_line, index) => {
            const line_data = {};

            if(input_line.A){
                line_data.narr = input_line.A;
            }else{
                console.error('ERROR:: closing sheet A열 누락');
                line_data.narr = '';
            }
    
            if(input_line.B){
                line_data.dividedbooknumber = input_line.B;
            }else{
                console.error('ERROR:: closing sheet B열 누락');
                line_data.dividedbooknumber = '';
            }
    
            if(input_line.C){
                line_data.currentdivided = input_line.C;
            }else{
                console.error('ERROR:: closing sheet C열 누락');
                line_data.currentdivided = '';
            }
            
            if(input_line.D){
                line_data.closingment = input_line.D;
            }else{
                console.error('ERROR:: closing sheet D열 누락');
                line_data.closingment = '';
            }
            
            if(input_line.E){
                line_data.closingaudio = input_line.E;
            }else{
                console.error('ERROR:: closing sheet E열 누락');
                line_data.closingaudio = '';
            }
            target_data = line_data;
        });

        return target_data;
    }
}

var myArgs = process.argv.slice(2);
const excelFile =myArgs[0];

fs.readdir(myArgs[0], function(error, filelist){
    console.log(filelist)
    filelist.forEach(function(element){
        getDataSheet(excelFile+'\\'+element,excelFile+'\\'+element.replace('.xls',''));
    });
})


