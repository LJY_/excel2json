// 'use strict';
const excelToJson = require('convert-excel-to-json');
var jsonfile = require('jsonfile');

//엑셀파일
var fs = require('fs');

function getDataSheet(excelFile, filePath){
    console.log(excelFile, filePath)
    const excel_sheets = excelToJson({
        sourceFile: excelFile,
        header: {
            rows: 2 //몇번째 줄부터 시작인지?
        },
        //원하는 시트만 골라서 뽑아내기
        sheets: ['Sheet1']
    });

    //input_data에 모든 시트의 데이터를 삽입
    let input_data = [];
    input_data = input_data.concat(getDataListFromSheet("111", excel_sheets.Sheet1));
    
    if (!fs.existsSync(filePath)){
        fs.mkdirSync(filePath);
    }

    jsonfile.writeFile(filePath+'/tableList.json', input_data);

    return input_data;
}

//시트별 JSON 재구성
function getDataListFromSheet(sheet_name, input_sheet1){
    const target_data = [];
    const regex = /page/gi;
    
    //한 줄 분의 데이터 작성
    input_sheet1.forEach(input_line => {
        const line_data = {};

        if(input_line.A !== undefined){
            line_data.num = input_line.A;
        }
        if(input_line.B !== undefined){
            line_data.bookName = input_line.B;
        }
        if(input_line.C !== undefined){
            line_data.writer = input_line.C;
        }
        if(input_line.D !== undefined){
            line_data.publisher = input_line.D;
        }
        if(input_line.F !== undefined){
            line_data.isbn = input_line.F;
        }
        if(input_line.G !== undefined){
            line_data.audio = true;
        }else{
            line_data.audio = false;
        }
        console.log(input_line.A +' '+ input_line.G)
        //한 줄 분의 데이터를 시트 전체의 데이터에 삽입
        target_data.push(line_data);
    });
    
    // console.log(target_data);
    return target_data;
}

var myArgs = process.argv.slice(2);
const excelFile =myArgs[0];

fs.readdir(myArgs[0], function(error, filelist){
    console.log(filelist)
    filelist.forEach(function(element){
        getDataSheet(excelFile+'\\'+element,excelFile+'\\'+element.replace('.xls',''));
    });
})


